package cn.archko.microblog.sliding.app;

/**
 * @author: archko Date: 13-1-19 Time: 上午10:16
 * @description:
 */
public interface SlidingMenuChangeListener {

    void showMenu(int pos);
}
