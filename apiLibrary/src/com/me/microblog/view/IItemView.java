package com.me.microblog.view;

/**
 * @description:
 * @author: archko 11-9-5
 */
public interface IItemView{

    void update(Object object, boolean bool1, boolean bool2, int i);

}
