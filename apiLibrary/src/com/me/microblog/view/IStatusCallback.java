package com.me.microblog.view;

/**
 * @version 1.00.00
 * @description: ActionBar的回调方法。
 * @author: archko 11-12-8
 */
public interface IStatusCallback {

    void refresh();
}
